package org.dnim.wiley.twolevelcache;

import org.dnim.wiley.twolevelcache.cache.CacheManager;
import org.dnim.wiley.twolevelcache.book.*;
import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * This class tests cache usage by creating CacheUser (Reader) and run them in the separate threads.
 * 
 * @author Sergey Korsik
 */
public class CacheUsageTest{

    private static final int USER_AMOUNT = 5;
    
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Start CachManager testing...");
        CacheManager cacheManager = initializeCacheManager();
        Library library = new Library(cacheManager);

        for (int i=0; i < 10; i++){
            Reader reader = new Reader(library, 100, "reader-" + i);
            new Thread(reader).start();
        }    

        System.out.println("CachManager testing has finised.");
    }

    /**
     * Parses config file and provide and initialize CacheManager based on the retrieved data
     */
    private static CacheManager initializeCacheManager() throws IOException {
        InputStream propertiesIn = ClassLoader.getSystemResourceAsStream("config.properties");
        Properties properties = new Properties();
        properties.load(propertiesIn);
        int cacheLevel = Integer.parseInt(properties.getProperty("cacheLevel", "0").trim());
        int firstLevelCacheSize = Integer.parseInt(properties.getProperty("firstLevelCacheSize", "0").trim());
        int secondLevelCacheSize = Integer.parseInt(properties.getProperty("secondLevelCacheSize", "0").trim());
        System.out.println("properties loaded (cacheLevel): " + cacheLevel);
        System.out.println("properties loaded (firstLevelCacheSize): " + firstLevelCacheSize);
        System.out.println("properties loaded (secondLevelCacheSize): " + secondLevelCacheSize);

        return new CacheManager(cacheLevel, firstLevelCacheSize, secondLevelCacheSize);
    }

}
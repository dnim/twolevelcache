package org.dnim.wiley.twolevelcache.book;

import org.dnim.wiley.twolevelcache.cache.Cacheable;

/**
 * Cacheable object implementation. 
 *
 * @author Sergey Korsik
 */
public class Book implements Cacheable {

    private String isbn;

    public Book(String isbn){
        this.isbn = isbn;
    }

    @Override
    public String getId(){
        return this.isbn;
    }
}
package org.dnim.wiley.twolevelcache.book;

/**
 * Reader randomly put or get book from library
 * 
 * @author Sergey Korsik
 */
public class Reader implements Runnable {

    private final Library library;
    private final int amountOfIterations;
    private final String readerName;

    public Reader(Library library, int amountOfIterations, String readerName){
        this.library = library;
        this.amountOfIterations = amountOfIterations;
        this.readerName = readerName;
    }

    public void run() {
        for (int i = 0; i < amountOfIterations; i++){
            String readerPrefix = "READER '" + readerName + "'";
            String bookId = "bookISBN-" + i;
            boolean put = new Double(Math.random()*2).intValue() == 0 ? true : false;
            if (put){
                System.out.println(readerPrefix + " > PUT book with id: '" + bookId + "'");
                library.putBook(new Book(bookId));
            } else {
                System.out.println(readerPrefix + " < GET book with id: '" + bookId + "'");
                library.getBook(bookId);
            }
            try{
                Thread.sleep(100);      
            } catch(InterruptedException e){
                System.out.println("ERROR: thread was unexpectedly interrupted.");
            }
        }  
    }
}
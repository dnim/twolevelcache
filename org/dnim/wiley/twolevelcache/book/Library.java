package org.dnim.wiley.twolevelcache.book;

import org.dnim.wiley.twolevelcache.cache.Cache;

/**
 * This layer (Library) represents abstraction between end user (Reader) 
 * and low layers such as cache, database (not implemented in this realization)
 * 
 * @author Sergey Korsik 
 */
public class Library {

    private final Cache cacheManager;

    public Library(Cache cacheManager){
        this.cacheManager = cacheManager;
    }

    public Book getBook(String bookId){
        return (Book)cacheManager.get(bookId);
    }

    public void putBook(Book book){
        cacheManager.put(book);
    }
}
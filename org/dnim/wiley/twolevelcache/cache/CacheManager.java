package org.dnim.wiley.twolevelcache.cache;

import java.io.IOException;

/**
 * This class is facade around MemoryCacheManager and FileSystemCacheManager.
 * 
 * The main goal is to provide simple mechanism to the end user
 * 
 * @author Sergey Korsik
 */
public class CacheManager implements Cache {

    private static final int DEFAULT_CACHE_LEVEL = 1;
    private static final int CACHE_LEVEL_MAX_SIZE = 2;
    private static final int CACHE_LEVEL_MIN_SIZE = 2;

    private static final int DEFAULT_1ST_LEVEL_CACHE_SIZE = 10;
    private static final int DEFAULT_2ND_LEVEL_CACHE_SIZE = 10;

    private static final int FIRST_LEVEL_CACHE_MAX_SIZE = 1000;
    private static final int SECOND_LEVEL_CACHE_MAX_SIZE = 1000;

    private static final int FIRST_LEVEL_CACHE_MIN_SIZE = 1;
    private static final int SECOND_LEVEL_CACHE_MIN_SIZE = 1;

    private final Object lock = new Object();

    private final int cacheLevel;
    private final int firstLevelSize;
    private final int secondLevelSize;

    private final Cache memoryCacheManager;
    private final Cache fileSystemCacheManager;

    public CacheManager(int cacheLevel, int firstLevelSize, int secondLevelSize) throws IOException {
        System.out.println("Init CacheManager");
        if (cacheLevel < CACHE_LEVEL_MIN_SIZE || cacheLevel > CACHE_LEVEL_MAX_SIZE){
            this.cacheLevel = DEFAULT_CACHE_LEVEL;
        } else {
            this.cacheLevel = cacheLevel;
        }
        if (firstLevelSize < FIRST_LEVEL_CACHE_MIN_SIZE || firstLevelSize > FIRST_LEVEL_CACHE_MAX_SIZE){
            this.firstLevelSize = DEFAULT_1ST_LEVEL_CACHE_SIZE;
        } else {
            this.firstLevelSize = firstLevelSize;
        }
        // don't need to initialize 2nd level cache if cache level is 1
        if (cacheLevel == 1){
            this.secondLevelSize = -1;
        } else if (secondLevelSize < SECOND_LEVEL_CACHE_MIN_SIZE || secondLevelSize > SECOND_LEVEL_CACHE_MAX_SIZE){
            this.secondLevelSize = DEFAULT_2ND_LEVEL_CACHE_SIZE;
        } else {
            this.secondLevelSize = secondLevelSize;
        }
        // configure caches
        memoryCacheManager = new MemoryCacheManager(firstLevelSize);
        if (cacheLevel == 2){
            fileSystemCacheManager = new FileSystemCacheManager(secondLevelSize);
        } else {
            fileSystemCacheManager = null;
        }
        System.out.println("CacheManager initialized with the next parameters:\n" + 
            "\tcacheLevel: " + cacheLevel + "\n" + 
            "\tfirstLevelSize: " + firstLevelSize + "\n" + 
            "\tsecondLevelSize: " + secondLevelSize);
    }

    @Override
    public Cacheable put(Cacheable cacheableObject){
        System.out.println("\t> Cache Manager PUT");
        String id = cacheableObject.getId();
        Cacheable result = null;
        synchronized(lock) {
            if (!(memoryCacheManager.contains(id) || 
               (cacheLevel == 2 && fileSystemCacheManager.contains(id)))){
                result = memoryCacheManager.put(cacheableObject);
                if (result != null && cacheLevel == 2){
                    result = fileSystemCacheManager.put(result);
                }
            } else {
                System.out.println("\t\tObject with id:'" + id + "' is already cached.");
            }

        }
        return result;
    }

    @Override
    public Cacheable get(String objectId){
        System.out.println("\t< Cache Manager GET");
        Cacheable result = null;
        synchronized(lock) {
            result = memoryCacheManager.get(objectId);
            if (result != null){
                return result;
            }
            if (cacheLevel == 2){
                result = fileSystemCacheManager.get(objectId);
                if (result == null){
                    return result;
                }
                // refresh object relevance by pushing it from the file system cache to memory cache
                Cacheable lruObject = memoryCacheManager.put(result);
                fileSystemCacheManager.put(lruObject);
            }
        }
        return result;
    }

    @Override
    public boolean contains(String id){
        return (memoryCacheManager.contains(id) || 
               (cacheLevel == 2 && fileSystemCacheManager.contains(id)));
    }
}
package org.dnim.wiley.twolevelcache.cache;

import java.io.IOException;

/**
 * Represents cache
 * 
 * @author Sergey Korsik
 */
public interface Cache {

    /**
     * @param cacheableObject object to persist in the cache
     *                       
     * @return <tt>null</tt> in case if object with the same id already cached
     *         or if there is enough space in cache (i.e. amount of object in cache less than cache size),
     *         otherwise return least recently used (LRU) cacheable object (pop it in case if cache is full)
     */
    Cacheable put(Cacheable cacheableObject);

    /**
     * @param id object identifier
     *
     * @return cached object if presented in the cache, <tt>null</tt> otherwise
     */
    Cacheable get(String id);

    /**
     * @param id object identifier
     * 
     * @return <tt>true</tt> if object with provided id exists in the cache <tt>false</tt> otherwise.
     */ 
    boolean contains(String id);

}


package org.dnim.wiley.twolevelcache.cache;

import java.io.Serializable;

/**
 * Represents cacheable object. 
 * Each object should be serializable as far as it can be saved in the second level cache (file system)
 *
 * @author Sergey Korsik
 */
public interface Cacheable extends Serializable {

    String getId();

}
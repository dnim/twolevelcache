package org.dnim.wiley.twolevelcache.cache;

import java.util.LinkedHashMap;

/**
 * This class provide in-memory cache handling
 *
 * This is a simple implementation of LRUMap
 *  
 * @author Sergey Korsik
 */    
class MemoryCacheManager implements Cache {

    private final int cacheSize;

    private int elementsCounter;

    private final LinkedHashMap<String, Cacheable> cache;

    MemoryCacheManager(int cacheSize){
        this.cacheSize = cacheSize;
        // Linked hash map has capacity power of 2. So cacheSize sets rough map capacity. 
        cache = new LinkedHashMap<String, Cacheable>(cacheSize, 1);
    }

    @Override
    public Cacheable put(Cacheable cacheableObject){
        Cacheable result = null;
        String id = cacheableObject.getId();
        System.out.print("\t\t> MEM: ");
        if (cache.containsKey(id)){
            System.out.println("Object with id:'" + id + "' is already cached.");
        } else if (elementsCounter < cacheSize){
            cache.put(id, cacheableObject);
            elementsCounter++;
            System.out.println("Object added to the cache:'" + id + 
                "'. Amount of objects in the cache: (" + elementsCounter + "/" + cacheSize + ").");
        } else {
            String lruObjectId = cache.keySet().iterator().next();
            result = cache.remove(lruObjectId);
            cache.put(id, cacheableObject);
            System.out.println("Object added to the cache:'" + id + 
                "'. Less relevant object removed from the cache: '" + lruObjectId + "'");            
        }
        return result;
    }

    @Override
    public Cacheable get(String objectId){
        Cacheable result = null;
        System.out.print("\t\t< MEM: ");
        if (cache.containsKey(objectId)){
            // add id,value to the end to up object's relevance
            result = cache.remove(objectId);
            cache.put(objectId, result);
            System.out.println("Object with id:'" + objectId + "' retrieved from the cache. Made the most relevant.");
        } else {
            System.out.println("Object with id:'" + objectId + "' is not presented in the cache.");
        }
        return result;        
    }

    @Override
    public boolean contains(String objectId){
        return cache.containsKey(objectId);
    }
}
package org.dnim.wiley.twolevelcache.cache;

import java.nio.file.Files;
import java.io.*;
import java.util.LinkedList;


/**
 * File system cache. Responsible for serialization and deserializaion 
 *  
 * @author Sergey Korsik
 */
class FileSystemCacheManager implements Cache {

    private final int cacheSize;

    private int elementsCounter = 0;
    
    private File cacheDir;

    // handles ids of objects in file system cache
    private final LinkedList<String> idsList = new LinkedList<String>();

    FileSystemCacheManager(int cacheSize) throws IOException {
        this.cacheSize = cacheSize;
        initializeCacheDir();
        System.out.println("<< cache dir was initialized: " + cacheDir.getCanonicalPath());
    }

    @Override
    public Cacheable put(Cacheable cacheableObject) {
        Cacheable result = null;
        String id = cacheableObject.getId();
        System.out.print("\t\t> FS: ");
        if (idsList.contains(id)){
            System.out.println("Object with id:'" + id + "' is already cached.");
        } else if (elementsCounter < cacheSize){
            serializeObject(cacheableObject);
            idsList.add(id);
            elementsCounter++;
            System.out.println("Object added to the cache:'" + id + 
                "'. Amount of objects in the cache: (" + elementsCounter + "/" + cacheSize + ").");
        // if cache is full
        } else {
            // get the least recently used object
            String oldObjectId = idsList.removeFirst();
            result = deserializeObject(oldObjectId);
            removeObject(oldObjectId);
            serializeObject(cacheableObject);            
            idsList.add(id);
            System.out.println("Object added to the cache:'" + id + 
                "'. Less relevant object removed from the cache: '" + oldObjectId + "'");
        }
        return result;
    }

    /**
     * Removes object from the cache to push it on the high level cache (memory cache)
     *
     * @param key 
     *           Cacheable object id
     * @return Cacheable object if it present, <tt>null</tt> - otherwise.
     * 
     */ 
    @Override
    public Cacheable get(String objectId) {
        Cacheable result = null;
        System.out.print("\t\t< FS: ");
        if (idsList.contains(objectId)){
            result = deserializeObject(objectId);
            removeObject(objectId);
            elementsCounter--;
            idsList.remove(objectId);
            System.out.println("Object with id:'" + objectId + "' retrieved from the cache. Removed from the cache.");
        } else {
            System.out.println("Object with id:'" + objectId + "' is not presented in the cache.");
        }
        return result;
    }

    @Override
    public boolean contains(String objectId){
        return idsList.contains(objectId);
    }

    private void removeObject(String objectId){
        File objectFile = new File(cacheDir, objectId);
        if(!objectFile.delete()){
            handleException(new RuntimeException("Object with id: " + objectId + "can't be deleted from the cache"));
        }
    }
    
    private Cacheable deserializeObject(String objectId) {
        File objectFile = new File(cacheDir, objectId);
        Cacheable deserializedObject = null;
        try (FileInputStream fileIn = new FileInputStream(objectFile);
             ObjectInputStream objectIn = new ObjectInputStream(fileIn);) 
        {
            deserializedObject = (Cacheable) objectIn.readObject();
        } catch (IOException | ClassNotFoundException e){
            handleException(e);
        }
        return deserializedObject;
    }

    private void serializeObject(Cacheable cacheableObject) {
        File objectFile = new File(cacheDir, cacheableObject.getId());
        try (FileOutputStream fileOut = new FileOutputStream(objectFile);
             ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);) 
        {
            objectOut.writeObject(cacheableObject);
        } catch (IOException e){
            handleException(e);
        }
    }
    
    private void initializeCacheDir() {
        System.out.println(">> initialize cache dir");
        try {
            cacheDir = Files.createTempDirectory("cacheDir").toFile();
        } catch(IOException e) {
            handleException(e);
        }
    }

    /**
     * This is simplified exception handling 'system' that converts checked exceptions to unchecked.
     */
    private void handleException(Exception e){
        throw new RuntimeException("Something went wrong.", e);

    }
}
### TASK DESCRIPTION ###

Create a configurable two-level cache (for caching Objects).  Level 1 is memory, 
level 2 is filesystem. Config params should let one specify the cache strategies 
and max sizes of level  1 and 2.

### PROJECT SUMMERY ###

* Version: 0.1

### SETUP AND RUN ###

* Configuration
    * Use `config.properties` file to configure cache levels, caches' sizes.
* Run
* Compile

### TODO list ###

* 
* 
* 

### Additional info ###
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)